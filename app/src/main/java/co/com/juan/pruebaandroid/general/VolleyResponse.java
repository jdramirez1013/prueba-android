package co.com.juan.pruebaandroid.general;

import com.android.volley.VolleyError;

import org.json.JSONObject;

public interface VolleyResponse {
    void onRsponse(JSONObject jsonObject);
    void onError(VolleyError error);
}
