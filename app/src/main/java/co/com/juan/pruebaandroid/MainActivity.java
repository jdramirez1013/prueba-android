package co.com.juan.pruebaandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.android.volley.VolleyError;

import org.json.JSONObject;

import co.com.juan.pruebaandroid.general.VolleyResponse;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Volley volley = new Volley(this);

        volley.login("user", "contra", new VolleyResponse() {
            @Override
            public void onRsponse(JSONObject jsonObject) {

            }

            @Override
            public void onError(VolleyError error) {

            }
        });

    }
}
