package co.com.juan.pruebaandroid;

import android.content.Context;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import co.com.juan.pruebaandroid.general.VolleyManager;
import co.com.juan.pruebaandroid.general.VolleyResponse;

public class Volley {

    VolleyManager volleyManager;
    Context context;

    public Volley(Context context) {
        this.context = context;
        volleyManager = VolleyManager.getInstance(context);
    }


    public void login(String user, String pass, VolleyResponse callback){
        String service = context.getString(R.string.app_name);
        try {
            JSONObject data = new JSONObject();
            data.put("user", user);
            data.put("pass", pass);

            volleyManager.requestPost(String.format("%s/login", service), data, callback);
        } catch (JSONException e) {
            callback.onError(new VolleyError("error"));
        }
    }
}
